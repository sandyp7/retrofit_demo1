package com.actech.retrofit_demo1.network;

import com.actech.retrofit_demo1.model.RetroPhoto;

import java.util.List;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;

public class RetrofitClientInstance {

    private static Retrofit retrofit;
    private static final String BASE_URL = "https://firebasestorage.googleapis.com";

    public static Retrofit getRetrofitInstance() {
        if (retrofit == null) {
            retrofit = new retrofit2.Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
    public interface GetDataService {

        @GET("/v0/b/fashionpointgarments-216416.appspot.com/o/sample.json?alt=media&token=1421055c-1844-4db1-a1c4-5f55e1ec95d0")
        Call<List<RetroPhoto>> getAllPhotos();
    }
}
